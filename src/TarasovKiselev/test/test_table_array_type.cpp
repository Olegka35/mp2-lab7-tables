#include "../gtest/gtest.h"
#include "../include/TSortTable.h""

TEST(TScanTable, Can_create_ScanTable) {
	EXPECT_NO_FATAL_FAILURE(TScanTable table(5));
}

TEST(TScanTable, Can_Ins_Record) {
	TScanTable* table = new TScanTable(5);
	int i = 5;
	TTabRecord rec("Kiselev I.", (TDatValue*)&i);	
	table->InsRecord("Kiselev I.", (TDatValue*)&i);
	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TScanTable, Can_Ins_two_records) {
	TScanTable* table = new TScanTable(5);
	int i = 5;
	table->InsRecord("Kiselev I.", (TDatValue*)&i);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Kiselev D.", (TDatValue*)&i));
}

TEST(TScanTable, Can_find_record) {
	TScanTable* table = new TScanTable(5);
	int* i= new int (1);
	i[0] = 5;
	table->InsRecord("Kiselev I.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Mamai", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("Smirnov N.", (TDatValue*)i);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Gorojanin L.", (TDatValue*)i);

	EXPECT_EQ(2 , *((int*)(table->FindRecord("Mamai"))));
}

TEST(TScanTable, Can_del_record) {
	TScanTable* table = new TScanTable(5);
	int *i = new int(1);
	i[0] = 5;
	table->InsRecord("Kiselev I.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 2;
	table->InsRecord("Mamai", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 3;
	table->InsRecord("Smirnov N.", (TDatValue*)&i[0]);
	i = new int(1);
	i[0] = 4;
	table->InsRecord("Gorojanin L.", (TDatValue*)&i[0]);

	EXPECT_NO_FATAL_FAILURE(table->DelRecord("Mamai"));
	EXPECT_TRUE(nullptr == table->FindRecord("Mamai"));
}

TEST(TSortTable, can_create_table) {
	EXPECT_NO_FATAL_FAILURE(TSortTable table(5));
}

TEST(TSortTable, can_Ins_Record) {
	TSortTable* table = new TSortTable(5);
	int i = 5;
	TTabRecord rec("Kiselev I.", (TDatValue*)&i);
	table->InsRecord("Kiselev I.", (PTDatValue)&i);

	EXPECT_TRUE(rec == *table->GetCurrRecord());
}

TEST(TSortTable, can_assignment_table) {
	TSortTable* table = new TSortTable(5);
	int i = 5;
	table->InsRecord("Kiselev I.", (PTDatValue)&i);
	TSortTable table2(2);

	table2 = *table;

	EXPECT_TRUE(table2.GetCurrRecord(), table->GetCurrRecord());
}

TEST(TSortTable, can_set_and_get_sort_metod) {
	TSortTable* table = new TSortTable(5);
	int i = 5;
	table->SetSortMethod(INSERT_SORT);

	EXPECT_TRUE(table->GetSortMethod() == INSERT_SORT);
}

TEST(TSortTable, can_used_all_sort___can_find_rec) {
	TSortTable* table = new TSortTable(5);
	int* i = new int(1);
	i[0] = 5;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Kiselev I.", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(MERGE_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Mamai", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 3;
	table->SetSortMethod(QUICK_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Smirnov N.", (TDatValue*)&i[0]));
	i = new int(1);
	i[0] = 4;
	table->SetSortMethod(INSERT_SORT);
	EXPECT_NO_FATAL_FAILURE(table->InsRecord("Gorojanin L.", (TDatValue*)&i[0]));

	EXPECT_EQ(4, *((int*)(table->FindRecord("Mamai"))));
}
